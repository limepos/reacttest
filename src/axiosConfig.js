// First we need to import axios.js
import axios from 'axios';
// Next we make an 'instance' of it
const access_token = localStorage.getItem('token');
axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}` 
const instance = axios.create({
// .. where we make our configurations
    baseURL: 'http://localhost:8000'
});
export default instance;