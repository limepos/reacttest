import React, { useState, Component } from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar, UsersTable } from './components';
import mockData from './data';
import axiosConfig from '../../axiosConfig'; 

class UserList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user : mockData
    }
     this.getUser();
  }

   getUser =()=> {
    axiosConfig.get("/users")
    .then(response => {
       console.log("response",response.data);
        this.setState({
          user: response.data.data
        })
    }).catch(error => {
      console.log("Login Error.",error)
     });
   }

  render(){
    return (
      <div>
      <UsersToolbar />
       <div>
        <UsersTable users={this.state.user} />
      </div>
    </div>
    );
  }
}

export default UserList;

