import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import InputLabel from '@material-ui/core/InputLabel';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axiosConfig from '../../../../axiosConfig'; 
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import CloseIcon from '@material-ui/icons/Close';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  FormHelperText,
  Checkbox,
  Typography
} from '@material-ui/core';

import { SearchInput } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingBottom: 30,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signUpButton: {
    margin: theme.spacing(2, 0)
  },
  formControl: {
    margin: 0,
    marginTop : 16,
    minWidth: 120,
    width : "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  close : {
    position : "absolute",
    right : 20,
    top : 20

  }
}));
const schema = {
  first_name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  last_name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  type: {
    presence: { allowEmpty: true },
    length: {
      maximum: 128
    }
  }

};
const UsersToolbar = props => {
  const { className, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };
  
  const [age, setAge] = React.useState('');

  const handleChangeType = (event) => {
    setAge(event.target.value);
    
    formState.values.id_tipouser = event.target.value;

  };

  const handleSignUp = event => {
    event.preventDefault();
    console.log(formState.values)
    axiosConfig.post('/register', formState.values)
    .then(response => {
      handleClose()
      window.location.reload(false);
    }).catch(error => {
      console.log("Login Error.")
   })
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;
  return (
    <div style={{padding: 24}} >
      <div className={classes.row}>
      <Typography
                className={classes.quoteText}
                variant="h2"
              >  All Users </Typography>
        <span className={classes.spacer} />
        {localStorage.getItem('role') == 1 ? <Button
          color="primary"
          variant="contained"
          onClick={handleClickOpen}
          if={localStorage.getItem('role') == 1}
        > 
          Add user
        </Button> : ''}
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add New User</DialogTitle>
        <DialogContent>
        <a onClick={handleClose} className={classes.close}><CloseIcon/></a>
           <div className={classes.contentBody}>
              <form
                className={classes.form}
                onSubmit={handleSignUp}
              >
                <TextField
                  className={classes.textField}
                  error={hasError('first_name')}
                  fullWidth
                  helperText={
                    hasError('first_name') ? formState.errors.first_name[0] : null
                  }
                  label="First name"
                  name="first_name"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.first_name || ''}
                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
                  error={hasError('last_name')}
                  fullWidth
                  helperText={
                    hasError('last_name') ? formState.errors.last_name[0] : null
                  }
                  label="Last name"
                  name="last_name"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.last_name || ''}
                  variant="outlined"
                />
                 <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="demo-simple-select-outlined-label">User Type</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={age}
          onChange={handleChangeType}
          label="User Type"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1}>Admin</MenuItem>
          <MenuItem value={2}>User</MenuItem>
         
        </Select>
      </FormControl>
                <TextField
                  className={classes.textField}
                  error={hasError('email')}
                  fullWidth
                  helperText={
                    hasError('email') ? formState.errors.email[0] : null
                  }
                  label="Email address"
                  name="email"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.email || ''}
                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
                  error={hasError('password')}
                  fullWidth
                  helperText={
                    hasError('password') ? formState.errors.password[0] : null
                  }
                  label="Password"
                  name="password"
                  onChange={handleChange}
                  type="password"
                  value={formState.values.password || ''}
                  variant="outlined"
                />
                
               
                <Button
                  className={classes.signUpButton}
                  color="primary"
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Submit
                </Button>
                
              </form>
            </div>
        </DialogContent>
        <DialogActions>
          
        </DialogActions>
      </Dialog>
      </div>
    
    </div>
  );
};

UsersToolbar.propTypes = {
  className: PropTypes.string
};

export default UsersToolbar;
