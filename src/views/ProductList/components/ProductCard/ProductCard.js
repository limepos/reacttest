import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import validate from 'validate.js';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import CloseIcon from '@material-ui/icons/Close';
import {
  Button,
  TextField,
  Link,
  FormHelperText
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import axiosConfig from '../../../../axiosConfig'; 
const useStyles = makeStyles(theme => ({
  root: {},
  imageContainer: {
    height: 64,
    width: 64,
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  },
  bigIconyello: {
    fontSize : 50,
    color: "#3f51b5"
  },
  bigIconGreen: {
    fontSize : 50,
    color: "#17882a"
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingBottom: 30,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signUpButton: {
    margin: theme.spacing(2, 0)
  },
  formControl: {
    margin: 0,
    marginTop : 16,
    minWidth: 120,
    width : "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  close : {
    position : "absolute",
    right : 20,
    top : 20

  }
}));

const ProductCard = props => {
  const { className, product, user, ...rest } = props;

  const classes = useStyles();
  const schema = {
    ticket: {
      presence: { allowEmpty: false, message: 'is required' },
      length: {
        maximum: 32
      }
    },
    type: {
      presence: { allowEmpty: true },
      length: {
        maximum: 128
      }
    }
  
  };
  const [age, setAge] = React.useState('');
  const [status, setStatus] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const handleCloseTicket = () => {
    setOpen(false);
    console.log("ddd",open)
  };

    const handleClickOpen = (ticketdata) => {
    setOpen(true);
    formState.values.ticket = ticketdata.ticket 
    formState.values.ticket_id = ticketdata.ticket_id 
    setAge(ticketdata.User.user_id);
  };


  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };
  

  const handleChangeType = (event) => {
    setAge(event.target.value);
    
    formState.values.user_id = event.target.value;

  };

  const handleChangeStatus = (event) => {
    setStatus(event.target.value);
    
    formState.values.ticket_status = event.target.value;

  };

  const handleTicket = (event) => {
    event.preventDefault();

    console.log(formState.values);
    axiosConfig.put('/ticket/update/'+formState.values.ticket_id, formState.values)
    .then(response => {
      console.log(response);
      handleCloseTicket()
      window.location.reload(false);
    }).catch(error => {
      console.log("Login Error.",error)
   })
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
     
    >
      <Typography variant="h1"  align="center" component="h2" gutterBottom  onClick={()=>handleClickOpen(product)}>
        <DoneAllIcon className={product.ticket_status == 1 ? classes.bigIconyello : classes.bigIconGreen }/>
      </Typography>
      <CardContent>
         
         <Typography
          align="center"
          gutterBottom
          variant="h4"
          onClick={()=>handleClickOpen(product)}
        >
          {product.ticket}
        </Typography>
       
      </CardContent>
      <Divider />
      <CardActions  onClick={()=>handleClickOpen(product)}>
        <Grid
          container
          justify="space-between"
        >
          <Grid
            className={classes.statsItem}
            item
          >
            <AccessTimeIcon className={classes.statsIcon} />
            <Typography
              display="inline"
              variant="body2"
            >
              {moment(product.createdAt).format('DD/MM/YYYY')}
            </Typography>
          </Grid>
          <Grid
            className={classes.statsItem}
            item
          >
              <PersonOutlineIcon  className={classes.statsIcon}/> 
             <Typography
              display="inline"
              variant="body2"
            >
                 {product.User.first_name} {product.User.last_name}
            </Typography>
          </Grid>
        </Grid>
      </CardActions>
      <Dialog open={open} onClose={handleCloseTicket} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Update Ticket </DialogTitle>
        <DialogContent>
        <a onClick={handleCloseTicket} className={classes.close}><CloseIcon/></a>
           <div className={classes.contentBody}>
              <form
                className={classes.form}
                onSubmit={handleTicket}
              >
                <TextField
                  className={classes.textField}
                  error={hasError('ticket')}
                  fullWidth
                  helperText={
                    hasError('ticket') ? formState.errors.ticket[0] : null
                  }
                  label="Ticket"
                  name="ticket"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.ticket || ''}
                  variant="outlined"
                />

              <TextField
                  name="ticket_id"
                  onChange={handleChange}
                  type="hidden"
                  value={formState.values.ticket_id || ''}
                  
                />
                
                {localStorage.getItem('role') == 1 ? <FormControl variant="outlined" className={classes.formControl}>
                
        <InputLabel id="demo-simple-select-outlined-label">Assign User</InputLabel>
       
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={age}
          onChange={handleChangeType}
          label="Assign User"
        >
         
          {user.map((user) => (
            <MenuItem key={user.user_id} value={user.user_id}>
              {user.first_name} {user.last_name}
            </MenuItem>
          ))}
         
        </Select>
      </FormControl>: "" }

      <FormControl variant="outlined" className={classes.formControl}>
                
                <InputLabel id="demo-simple-select-outlined-label">Ticket Status</InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={status}
                  onChange={handleChangeStatus}
                  label="Ticket Status"
                >
                  <MenuItem value="1">
                     Pending  
                  </MenuItem>
                  <MenuItem value="2">
                     Complete 
                  </MenuItem>
                  {localStorage.getItem('role') == 1 ? <MenuItem value="0">
                     Delete  
                  </MenuItem> : ""}

                </Select>
              </FormControl>
                <Button
                  className={classes.signUpButton}
                  color="primary"
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Update
                </Button>
                
              </form>
            </div>
        </DialogContent>
        <DialogActions>
          
        </DialogActions>
      </Dialog>
    </Card>
  );
};

ProductCard.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired
};

export default ProductCard;
