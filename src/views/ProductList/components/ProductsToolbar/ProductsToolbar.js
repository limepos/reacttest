import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import InputLabel from '@material-ui/core/InputLabel';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import CloseIcon from '@material-ui/icons/Close';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  FormHelperText,
  Checkbox,
  Typography
} from '@material-ui/core';
import { SearchInput } from 'components';
import axiosConfig from '../../../../axiosConfig'; 
const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingBottom: 30,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signUpButton: {
    margin: theme.spacing(2, 0)
  },
  formControl: {
    margin: 0,
    marginTop : 16,
    minWidth: 120,
    width : "100%"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  close : {
    position : "absolute",
    right : 20,
    top : 20

  }
}));

const ProductsToolbar = props => {
  const { className, user, ...rest } = props;
  console.log("user-->",user);
  const classes = useStyles();
  const schema = {
    ticket: {
      presence: { allowEmpty: false, message: 'is required' },
      length: {
        maximum: 32
      }
    },
    type: {
      presence: { allowEmpty: true },
      length: {
        maximum: 128
      }
    }
  
  };

  const [open, setOpen] = React.useState(false);


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };
  
  const [age, setAge] = React.useState('');

  const handleChangeType = (event) => {
    setAge(event.target.value);
    
    formState.values.user_id = event.target.value;

  };

  const handleTicket = (event) => {
    event.preventDefault();
    axiosConfig.post('/new-ticket', formState.values)
    .then(response => {
      console.log(response);
      handleClose()
      window.location.reload(false);
    }).catch(error => {
      console.log("Login Error.")
   })
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div style={{ padding: 24 }}
    >
      <div className={classes.row}>
      <Typography
                className={classes.quoteText}
                variant="h2"
              >  All Tickets </Typography>
        <span className={classes.spacer} />
        
       {localStorage.getItem('role') == 1 ? <Button
          color="primary"
          variant="contained"
          onClick={handleClickOpen}
          >
          Add Ticket 
        </Button> : "" }
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add New Ticket </DialogTitle>
        <DialogContent>
        <a onClick={handleClose} className={classes.close}><CloseIcon/></a>
           <div className={classes.contentBody}>
              <form
                className={classes.form}
                onSubmit={handleTicket}
              >
                <TextField
                  className={classes.textField}
                  error={hasError('ticket')}
                  fullWidth
                  helperText={
                    hasError('ticket') ? formState.errors.ticket[0] : null
                  }
                  label="Ticket"
                  name="ticket"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.ticket || ''}
                  variant="outlined"
                />
                
                 <FormControl variant="outlined" className={classes.formControl}>
                
        <InputLabel id="demo-simple-select-outlined-label">Assign User</InputLabel>
       
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={age}
          onChange={handleChangeType}
          label="User Type"
        >
         
          {user.map((user) => (
            <MenuItem key={user.user_id} value={user.user_id}>
              {user.first_name} {user.last_name}
            </MenuItem>
          ))}
         
        </Select>
      </FormControl>
                <Button
                  className={classes.signUpButton}
                  color="primary"
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Submit
                </Button>
                
              </form>
            </div>
        </DialogContent>
        <DialogActions>
          
        </DialogActions>
      </Dialog>
      </div>
      <div className={classes.row}>
      </div>
    </div>
  );
};

ProductsToolbar.propTypes = {
  className: PropTypes.string 
};

export default ProductsToolbar;
