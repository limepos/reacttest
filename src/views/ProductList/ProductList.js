import React, { useState, Component } from 'react';
import { makeStyles } from '@material-ui/styles';
import { ProductsToolbar, ProductCard } from './components';
import mockData from './data';
import axiosConfig from '../../axiosConfig'; 
import { IconButton, Grid, Typography } from '@material-ui/core';
class ProductList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      products : [],
      user : []
    }
     this.getTicket();
     this.getUser();
     
  }

   getTicket =()=> {

    if(localStorage.getItem('role') == 1){
    axiosConfig.get("/tickets")
    .then(response => {
       console.log("response",response.data);
        this.setState({
          products: response.data.data
        })
    }).catch(error => {
      console.log("Login Error.",error)
     });
    }else{
      axiosConfig.get("/tickets/"+localStorage.getItem('user_id'))
      .then(response => {
         console.log("response",response.data);
          this.setState({
            products: response.data.data
          })
      }).catch(error => {
        console.log("Login Error.",error)
       });
    }
   }

   getUser =()=> {
   axiosConfig.get("/users")
     .then(response => { 
       this.setState({
        user: response.data.data
      })
          console.log("response",this.state.user);
     }).catch(error => {
       console.log("Login Error.",error)
      });
    }

  render(){
    return (
      <div >
      <ProductsToolbar user={this.state.user} />
      <div style={{ padding: 24 }}>
        <Grid
          container
          spacing={3}
        >
          {this.state.products.map(product => (
            <Grid
              item
              key={product.id}
              lg={4}
              md={6}
              xs={12}
            >
              <ProductCard product={product} user={this.state.user} />
            </Grid>
          ))}
        </Grid>
      </div>
      
    </div>
    );
  }
}

export default ProductList;

