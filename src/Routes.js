import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';
import Auth from './views/Auth';
import {
  Dashboard as DashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView
} from './views';

console.log(Auth.getAuth());



const Routes = () => {
  if(localStorage.getItem('token')){
    Auth.authenticate();
  }
  const auth = Auth.getAuth();
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/dashboard"
      />
      <RouteWithLayout
        
        component={ auth ? DashboardView : SignInView }
        exact
        layout={ auth ? MainLayout : MinimalLayout }
        path="/dashboard"
      />
      <RouteWithLayout
        component={ auth ? UserListView : SignInView }
        exact
        layout={auth ? MainLayout : MinimalLayout}
        path="/users"
      />
      <RouteWithLayout
        component={ auth ? ProductListView : SignInView }
        exact
        layout={auth ? MainLayout : MinimalLayout }
        path="/ticket"
      />
      
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
